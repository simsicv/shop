<!DOCTYPE html>
<html lang="en">
  <head>
    <title>webShop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
  </head>
  <body>
    <!--css za class card-->
    <style>
      .card {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        max-width: 300px;
        max-height: 300px;
        margin: auto;
        text-align: center;
        border-radius: 5px;
      }
      .price {
        color: grey;
        font-size: 22px;
      }
      .card button {
        border: none;
        outline: 0;
        padding: 12px;
        color: white;
        background-color: #000;
        text-align: center;
        cursor: pointer;
        width: 100%;
        font-size: 18px;
      }
      .card button:hover {
        opacity: 0.7;
      }
    </style>
    <div class="header" style="sticky-header">
      <!-- Navigation -->
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
        <div class="container">
          <a class="navbar-brand" href="index.php">
            <img src="../Images/phones_img/nokia_phones_img/nokia_3310.jpg" style="height: 50px;"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item active">
                <a class="nav-link" href="index.php">Home<span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="log_reg.php">Login/Register</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="contact.php">Contact</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
    <div class="container">
      <h2>Ponuda</h2>
      <br>
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#home">Mobilni telefoni</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#menu1">Tableti</a>
        </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div id="home" class="container tab-pane active"><br>
          <h3>Mobilni telefoni</h3>
          <div class="col-xs-4 text-center">
            <!--prva kolona-->
            <div class="row">
              <div class="card">
                <h2>Samsung Galaxy S9</h2>
                <img src="./Images/phones_img/samsung_phones_img/galaxy_S9.jpg" style="width: 100px;">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
              <div class="card">
                <h2>Samsung Galaxy S9</h2>
                <img src="./Images/phones_img/samsung_phones_img/galaxy_A9.jpg" style="width: 100px;">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
              <div class="card">
                <h2>Samsung Galaxy S9</h2>
                <img src="./Images/phones_img/samsung_phones_img/galaxy_J7.jpg" style="width: 100px;">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
            </div>
            <!--kraj prve kolone-->
            <!--druga kolona-->
            <div class="row">
              <div class="card">
                <h2>Samsung Galaxy S9</h2>
                <img src="./Images/phones_img/samsung_phones_img/galaxy_S8.jpg" style="width: 100px;">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
              <div class="card">
                <h2>Samsung Galaxy S9</h2>
                <img src="./Images/phones_img/iphone_img/iphone_5S.jpg" style="width: 100px;">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
              <div class="card">
                <h2>Samsung Galaxy S9</h2>
                <img src="./Images/phones_img/iphone_img/iphone_6S.jpg" style="width: 100px;">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
            </div>
            <!--kraj druge kolone-->
            <!--treca kolona-->
            <div class="row">
              <div class="card">
                <h2>Samsung Galaxy S9</h2>
                <img src="./Images/phones_img/iphone_img/iphone_SE6.jpg" style="width: 100px;">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
              <div class="card">
                <h2>Samsung Galaxy S9</h2>
                <img src="./Images/phones_img/iphone_img/iphone_x.jpg" style="width: 100px;">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
              <div class="card">
                <h2>Samsung Galaxy S9</h2>
                <img src="./Images/phones_img/nokia_phones_img/nokia_3310.jpg" style="width: 100px;">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
            </div>
            <!--kraj trece kolone-->
            <!--cetvrta kolona-->
            <div class="row">
              <div class="card">
                <h2>Samsung Galaxy S9</h2>
                <img src="./Images/phones_img/nokia_phones_img/nokia_5210.jpg" style="width: 100px;">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
              <div class="card">
                <h2>Samsung Galaxy S9</h2>
                <img src="./Images/phones_img/nokia_phones_img/nokia_6600.jpg" style="width: 100px;">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
              <div class="card">
                <h2>Samsung Galaxy S9</h2>
                <img src="./Images/phones_img/nokia_phones_img/nokia_8210.jpg" style="width: 100px;">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
            </div>
            <!--kraj cetvrte kolone-->
            <!--peta kolona-->
            <div class="row">
              <div class="card">
                <h2>Samsung Galaxy S9</h2>
                <img src="./Images/phones_img/huawei_phones_img/huawei_mate_pro.jpg" style="width: 100px;">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
              <div class="card">
                <h2>Samsung Galaxy S9</h2>
                <img src="./Images/phones_img/huawei_phones_img/huawei_nn.jpg" style="width: 100px;">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
              <div class="card">
                <h2>Samsung Galaxy S9</h2>
                <img src="./Images/phones_img/huawei_phones_img/huawei_P8.jpg" style="width: 100px;">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
            </div>
            <!--kraj pete kolone-->
          </div>
          <div id="menu1" class="container tab-pane fade"><br>
            <h3>Tableti</h3>
            <div class="col-xs-3 text-center">
              <div class="row">
                <div class="card">
                  <h2>Tableti</h2>
                  <img src="./Images/tablets_img/apple_tablets_img/apple_1.jpg">
                  <p>299e</p>
                  <p class="price">
                    <button>Add to Cart</button>
                  </p>
                </div>
                <div class="card">
                  <h2>Tableti</h2>
                  <img src="./Images/tablets_img/apple_tablets_img/apple_3.jpg">
                  <p>299e</p>
                  <p class="price">
                    <button>Add to Cart</button>
                  </p>
                </div>
                <div class="card">
                  <h2>Tableti</h2>
                  <img src="./Images/tablets_img/apple_tablets_img/apple_4.jpg">
                  <p>299e</p>
                  <p class="price">
                    <button>Add to Cart</button>
                  </p>
                </div>
              </div>
              <div class="row">
                <div class="card">
                  <h2>Tableti</h2>
                  <img src="./Images/tablets_img/huawei_tablets_img/huawei_mpad_m1.jpg">
                  <p>299e</p>
                  <p class="price">
                    <button>Add to Cart</button>
                  </p>
                </div>
                <div class="card">
                  <h2>Tableti</h2>
                  <img src="./Images/tablets_img/huawei_tablets_img/huawei_mpad_m3.jpg">
                  <p>299e</p>
                  <p class="price">
                    <button>Add to Cart</button>
                  </p>
                </div>
                <div class="card">
                  <h2>Tableti</h2>
                  <img src="./Images/tablets_img/huawei_tablets_img/huawei_mpad_m3.jpg">
                  <p>299e</p>
                  <p class="price">
                    <button>Add to Cart</button>
                  </p>
                </div>
              </div>
              <div class="row">
                <div class="card">
                  <h2>Tableti</h2>
                  <img src="./Images/tablets_img/huawei_tablets_img/huawei_tab.jpg">
                  <p>299e</p>
                  <p class="price">
                    <button>Add to Cart</button>
                  </p>
                </div>
                <div class="card">
                  <h2>Tableti</h2>
                  <img src="./Images/tablets_img/huawei_tablets_img/huawei_mediatab_m2.jpg">
                  <p>299e</p>
                  <p class="price">
                    <button>Add to Cart</button>
                  </p>
                </div>
                <div class="card">
                  <h2>Tableti</h2>
                  <img src="./Images/tablets_img/nokia_tablets_img/nokia_tab_1.jpg">
                  <p>299e</p>
                  <p class="price">
                    <button>Add to Cart</button>
                  </p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="card">
                <h2>Tableti</h2>
                <img src="./Images/tablets_img/nokia_tablets_img/nokia_tab_2.jpg">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
              <div class="card">
                <h2>Tableti</h2>
                <img src="./Images/tablets_img/nokia_tablets_img/nokia_tab_3.jpg">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
              <div class="card">
                <h2>Tableti</h2>
                <img src="./Images/tablets_img/nokia_tablets_img/nokia_tab_4.jpg">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
            </div>
            <div class="row">
              <div class="card">
                <h2>Tableti</h2>
                <img src="./Images/tablets_img/samsung_tablets_img/galaxy_tab_A8.jpg">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
              <div class="card">
                <h2>Tableti</h2>
                <img src="./Images/tablets_img/samsung_tablets_img/galaxy_tab_E9.jpg">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
              <div class="card">
                <h2>Tableti</h2>
                <img src="./Images/tablets_img/samsung_tablets_img/galaxy_tab_S2.jpg">
                <p>299e</p>
                <p class="price">
                  <button>Add to Cart</button>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div>
      <!--Footer-->
      <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
        <div class="container text-center">
          <small>&copy;webShop 555 333</small>
        </div>
      </footer>
    </body>
  </html>