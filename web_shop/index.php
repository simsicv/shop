<!DOCTYPE html>
<html lang="en">
  <head>
    <title>webShop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
  </head>
  <body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
      <div class="container">
        <a class="navbar-brand" href="index.php">
          <img src="../Images/phones_img/nokia_phones_img/nokia_3310.jpg" style="height: 50px;">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="index.php">Home<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="log_reg.php">Login/Register</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="contact.php">Contact</a>
            </li>
          </ul>
          <div class="col-lg-2" style="background-color: red;">
            <button type="button" class="btn btn-default btn-sm">
              <span><img src="svg/si-glyph-bed.svg"/></span>
              Shopping Cart
            </button>
          </div>
        </div>>
      </div>
    </div>
  </nav>
  <!-- Page Content -->
  <div class="jumbotron jumbotron-fluid">
    <div class="container">
      <h1>Web Shop</h1>
      <p>Najveći izbor tableta i mobilnih telefona</p>
    </div>
    <div class="container">
      <button type="button" class="btn btn-block" style="background-color: rgba(0,0,0,.075); border-color: rgba(0,0,0,.075);">
        <a href="ponuda.php">
          <h2>Ponuda</h2>
        </a>
      </button>
      <!-- <button type="button" class="btn btn-block" style="background-color: #adb5bd; border-color: #adb5bd;"> <a href="mobilni_telefoni.php">Tableti</a> </button> -->
    </div>
  </div>
  <div class="container-fluid" style="text-align: center">
    <div class="row" style="padding-bottom: 30px;">
      <div class="col-sm-3">
        <img src="./Images/phones_img/iphone_img/iphone_5S.jpg" style="padding-top: 320px;">
        <img src="./Images/phones_img/samsung_phones_img/galaxy_S9.jpg" style="padding-top: 30px;">
      </div>
      <div class="col-sm-3" style="background-color:rgba(0,0,0,.075);">
        <h4>Mobilni telefoni</h4>
        <p>A smartphone is a cellular telephone with an integrated computer and other features not originally associated with telephones, such as an operating system, web browsing and the ability to run software applications. The first smartphone was IBM's
          Simon, which was presented as a concept device -- rather than a consumer device -- at the 1992 COMDEX computer trade show. It was capable of sending emails and faxes, as well as keeping a calendar of events for the user, as opposed to simply making
          calls and sending messages. Consumer smartphones evolved away from personal digital assistants (PDAs) around the turn of the 21st century when devices such as the PalmPilot began to include wireless connectivity. Several manufacturers, including
          Nokia and Hewlett Packard, released devices in 1996 that were combinations of PDAs and typical cellphones that included early operating systems (OSes) and web browsing capabilities. BlackBerry released its first smartphones in the mid-2000s, and they
          became very popular with consumers and in the enterprise. Many of these early smartphones featured physical keyboards. Get to know the features that make up a smartphone. In 2007, LG released the Prada and Apple released the iPhone, the first
          smartphones to feature a touchscreen. HTC released its Dream smartphone a year later, which was the first to include Google's Android OS. Other major advancements in the history of smartphones include Sony's release of the Xperia Z5 Premium phone
          with a 4K resolution screen in 2015. Networking advancements in Wi-Fi and LTE have also progressed over the years, improving the connectivity of smartphones for faster use.</p>
      </div>
      <div class="col-sm-3" style="background-color:#adb5bd;">
        <h4>Tableti</h4>
        <p>A tablet is a wireless, portable personal computer with a touchscreen interface. The tablet form factor is typically smaller than a notebook computer, but larger than a smartphone. The idea of tablet computing is generally credited to Alan Kay of
          Xerox, who sketched out the idea in 1971. The first widely sold tablet computer was Apple Computer's Newton, which was not a commercial success. Technological advances in battery life, display resolution, handwriting recognition software, memory and
          wireless internet access have since made tablets a viable computing option. Today, the most common type of tablet is the slate style, like Apple's iPad, Microsoft's Surface or Amazon's Kindle Fire. External keyboards are available for most
          slate-style tablets, and some keyboards also function as docking stations for the devices. Other styles of tablets include: Convertible tablets. These typically have a display that rotates 180 degrees and can be folded to close, screen up, over an
          integrated hardware keyboard. Convertible models may allow user input through a variety of methods in addition to the hardware keyboard, including natural handwriting with a stylus or digital pen and typing through a screen-based software keyboard.
          Hybrid tablets. Sometimes referred to as convertible or hybrid notebooks, a hybrid is like a regular notebook, but has a removable display that functions independently as a slate. Rugged tablets. A slate-like model that is designed to withstand rough
          handling and extreme conditions. Rugged tablets are usually encased in a thick protective shell and have shock-protected hard drives.</p>
      </div>
      <div class="col-sm-3">
        <img src="/Images/tablets_img/apple_tablets_img/apple_3.jpg" style="padding-top: 220px;">
        <img src="/Images/tablets_img/samsung_tablets_img/galaxy_tab_S2.jpg" style="padding-top: 30px;">
      </div>
    </div>
  </div>
  <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
    <div class="container text-center">
      <small>&copy;webShop 555 333</small>
    </div>
  </footer>
</body>
</html>