<!DOCTYPE html>
<html lang="en">
  <head>
    <title>webShop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="./style.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
  </head>
  <body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
      <div class="container">
        <a class="navbar-brand" href="index.php">
          <img src="../Images/phones_img/nokia_phones_img/nokia_3310.jpg" alt="" style="height: 50px;"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="index.php">Home<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="log_reg.php">Login/Register</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="contact.php">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <ul class="nav nav-tabs">
      <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="tableti.php">Tableti</a>
      </li>
    </ul>
    <div class="tab-content">
      <div id="" class="container tab-pane"><br>
        <h3>HOME</h3>
        <div class="row" style="padding-top: 30px; padding-bottom: 30px;">
          <div class="col-sm-3"><img src="../Images/tablets_img/apple_tablets_img/apple_1.jpg" style="height: 150px;"></div>
          <div class="col-sm-3"><img src="../Images/tablets_img/apple_tablets_img/apple_1.jpg" style="height: 150px;"></div>
          <div class="col-sm-3"><img src="../Images/tablets_img/apple_tablets_img/apple_1.jpg" style="height: 150px;"></div>
          <div class="col-sm-3"><img src="../Images/tablets_img/apple_tablets_img/apple_1.jpg" style="height: 150px;"></div>
        </div>
        <div class="row" style="padding-top: 30px; padding-bottom: 30px;">
          <div class="col-sm-3"><img src="./Images/phones_img/samsung_phones_img/galaxy_S9.jpg" style="height: 150px;"></div>
          <div class="col-sm-3"><img src="./Images/phones_img/samsung_phones_img/galaxy_S9.jpg" style="height: 150px;"></div>
          <div class="col-sm-3"><img src="./Images/phones_img/samsung_phones_img/galaxy_S9.jpg" style="height: 150px;"></div>
          <div class="col-sm-3"><img src="./Images/phones_img/samsung_phones_img/galaxy_S9.jpg" style="height: 150px;"></div>
        </div>
        <div class="row" style="padding-top: 30px; padding-bottom: 30px;">
          <div class="col-sm-3"><img src="../Images/phones_img/nokia_phones_img/nokia_3310.jpg" style="height: 150px;"></div>
          <div class="col-sm-3"><img src="./Images/phones_img/samsung_phones_img/galaxy_S9.jpg" style="height: 150px;"></div>
          <div class="col-sm-3"><img src="../Images/phones_img/nokia_phones_img/nokia_3310.jpg" style="height: 150px;"></div>
          <div class="col-sm-3"><img src="./Images/phones_img/samsung_phones_img/galaxy_S9.jpg" style="height: 150px;"></div>
        </div>
      </div>
    </div>
    <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
      <div class="container text-center">
        <small>&copy;webShop 555 333</small>
      </div>
    </footer>
