<!DOCTYPE html>
<html>
<!--
  <style>
    #myContainer {
      width: 400px;
      height: 400px;
      position: relative;
      background: blue;
    }
    #myAnimation {
      width: 50px;
      height: 50px;
      position: absolute;
      background-color: red;
    }
  </style>
  <body>
    <p>
      <button onclick="myMove()">Click Me</button>
    </p>
    <div id="myContainer">
      <div id="myAnimation">W</div>
    </div>
    <script>
      function myMove() {
        var elem = document.getElementById("myAnimation");
        var pos = 0;
        var id = setInterval(frame, 10);
        function frame() {
          if (pos == 350) {
            clearInterval(id);
          } else {
            pos++;
            elem.style.bottom = pos + 'px';
            elem.style.right = pos + 'px';
          }
        }
      }
    </script>
  -->
    <style>
    body {
    /* essential styling */
    height: calc(100vh - 1em);
    background:
        linear-gradient(var(--angle, 0deg),
            #ff9800 50%, #3c3c3c 0);

    &:after { /* just to prettify stuff */

        /* use absolute positioning because
         * margin: 0 on body
         * breaks Edge for some reason */
        position: absolute;
        top: 0; right: 0; bottom: 0; left: 0;
        font: calc(16vmin + 3rem)/ 100vh Leckerli One,
            cursive;
        text-align: center;
        cursor: pointer;
        content: 'Click!';

        @supports (mix-blend-mode: screen) {
            /* in supports because
             * background: inherit
             * breaks Edge for some reason
             * (sorry, I had no better idea) */

            background: inherit;
            -webkit-background-clip: text;
            background-clip: text;
            color: transparent;
            filter: invert(1) grayscale(1) contrast(3)
        }
    }
}
</style>
    <script>
    var NF = 50;

var rID = null,f = 0;

function stopAni() {
  cancelAnimationFrame(rID);
  rID = null;
};

function update() {
  var k = ++f / NF;

  document.body.style.setProperty(
  '--angle',
  +(k * 180).toFixed(2) + 'deg');


  if (!(f % NF)) {
    f = f % (4 * NF);
    stopAni();
    return;
  }

  rID = requestAnimationFrame(update);
};

addEventListener('click', function (e) {
  if (!rID) update();
}, false);
</script>
  </body>
</html>
